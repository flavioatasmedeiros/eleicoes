var _ = require('lodash');
var moment = require('moment');
var numeral = require('numeral');

module.exports = {
  formatarNome: formatarNome,
  nascimento: nascimento,
  prettyJson: prettyJson,
  dinheiro: dinheiro
};


function nascimento(str) {
  return moment(str, 'DD/MM/YYYY');
}


function dinheiro(input) {
  return numeral(input).format('(0,0)').replace(/,/g, '.');
}


function prettyJson(input) {
  console.log(JSON.stringify(input, null, 4));
}


 // Baseado nisso: http://goncin.wordpress.com/2010/12/16/normalizando-nomes-proprios-com-php/
function formatarNome(str) {
  
  if (typeof str !== 'string') {
    return 'error';
  }

  var conectivos = [  'de', 'di', 'do', 'da', 'dos', 'das', 'dello', 'della',
    'dalla', 'dal', 'del', 'e', 'em', 'na', 'no', 'nas', 'nos', 'van', 'von', 'y'];

  function capitalizar(palavra) {
    if (conectivos.indexOf(palavra) > -1) {
      return palavra;
    }
    return palavra.replace(/(?:^|\s)\S/g, function(a) { 
      return a.toUpperCase(); 
    });
  }
  
  return str.toLowerCase()
    .replace(/\./g, '. ')
    .replace(/\s+/g, ' ')
    .split(' ')
    .map(capitalizar).join(' ');
}


function logData(callback, log) {
  return function(err, data) {
    var data = callback(err, data);
    if (log) console.log(data);
    return data;
  }
}


//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round

// Closure
(function(){

  /**
   * Decimal adjustment of a number.
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number}      The adjusted value.
   */
  function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }

})();
