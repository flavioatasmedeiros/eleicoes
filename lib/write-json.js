var writeFile = require('./write-file');

module.exports = function(filename, data) {
  var jsonStr = JSON.stringify(data, null, 4);
  return writeFile(filename, jsonStr);
};

