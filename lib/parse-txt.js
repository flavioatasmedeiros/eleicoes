var Parser = require('tsv').Parser;
var fs = require('fs');
var _ = require('lodash');
var glob = require('glob');
require('iconv-lite').extendNodeEncodings();

module.exports = parsePattern;

function parsePattern(pattern, mapper) {
  return _.reduce(glob.sync(pattern), function(result, filename) {
    return result.concat(parse(mapper, filename));
  }, []);
}

function parse(mapper, filename) {
  var data = fs.readFileSync(filename, 'win1252').trim();
  var csv = new Parser(';', {header: false});
  return _.map(csv.parse(data), mapper);
}
