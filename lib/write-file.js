var mkdirp = require('mkdirp');
var path = require('path');
var fs = require('fs');

module.exports = writeJson;

function writeJson(filename, data) {
  mkdirp.sync(path.dirname(filename));
  return fs.writeFileSync(filename, data);
}
