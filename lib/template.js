var fs = require('fs');
var nunjucks = require('nunjucks');
var _ = require('lodash');
var moment = require('moment');
var misc = require('./misc');
moment.lang('pt-BR');

module.exports = _.curry(template);

function template(arquivo, data) {
  
  var template = fs.readFileSync('./templates/pages/' + arquivo).toString();

  var lastUpdate = moment().format('D [de] MMMM [de] YYYY [às] H[h]mm');
  
  var html  = nunjucks.renderString(
    template, 
    _.extend({lastUpdate: lastUpdate, misc: misc}, 
      data||{})
  );

  return html;
}

