# Parse de candidatos
wget http://agencia.tse.jus.br/estatistica/sead/odsele/consulta_cand/consulta_cand_2014.zip
cd csv/candidatos
unzip -o ../../consulta_cand_2014.zip
cd ../../
rm consulta_cand_2014.zip


wget http://agencia.tse.jus.br/estatistica/sead/odsele/bem_candidato/bem_candidato_2014.zip
cd csv/bens
unzip -o ../../bem_candidato_2014.zip
cd ../../
rm bem_candidato_2014.zip


node scripts/parse-bens.js > json/bens.json
node scripts/parse-candidatos.js > json/candidatos.json
node scripts/despesas.js > json/despesas.json
node scripts/sort-total-bens.js > json/sorted-total-bens.json
node scripts/estatisticas.js > json/estatisticas.json

mkdir __build
mkdir __build/despesas
mkdir __build/estatisticas
mkdir __build/bens

# Compila template e cria html
node scripts/html-despesas.js > __build/despesas/index.html
node scripts/html-total-bens.js > __build/bens/index.html
node scripts/html-estatisticas.js > __build/estatisticas/index.html
node scripts/html-index.js > __build/index.html
