// var scripts = ['candidatos', 'bens', 'total-bens', 'media-bens-partidos', 'estatisticas'];

// scripts.forEach(function(name) {
//   require('./scripts/' + name);
// });

var _ = require('lodash');
var template = require('./lib/template');
var writeFile = require('./lib/write-file');


writeFile('__build/index.html', template('index.html', {isIndex: true}));


writeFile('__build/candidatos/index.html', template('candidatos.html', {
  candidatos: _.take(require('./json/candidatos'), 100),
  bens: require('./json/total-bens')
}));


writeFile('__build/bens/index.html', template('bens.html', {
  candidatos: (function() {
    var totalBens = require('./json/total-bens');
    var candidatos = _.map(require('./json/candidatos'), function(candidato) {
      candidato.total_bens = totalBens[candidato.id];
      return candidato;
    });
    candidatos = _.filter(candidatos, function(candidato) {
      return candidato.total_bens > 0;
    });
    return _.take(_.sortBy(candidatos, 'total_bens').reverse(), 100);
  })()
}));


writeFile('__build/estatisticas/index.html', template('estatisticas.html', {
  estatisticas: require('./json/estatisticas')
}));

writeFile('__build/media-bens-partidos/index.html', template('media-bens-partidos.html', {
  partidos: require('./json/media-bens-partidos')
}));
