var _ = require('lodash');
var parseTxt = require('../lib/parse-txt');
var writeJson = require('../lib/write-json');
var misc = require('../lib/misc')
var nome = misc.formatarNome;

var candidatos = parseTxt('csv/candidatos/*.txt', function(row) {
  return {
    id:            parseInt(row[11], 10),
    nome_urna:     nome(row[13]),
    numero:        parseInt(row[12], 10),
    cargo:         nome(row[9]),
    estado:        row[6],
    partido:       row[17],
    situacao:      nome(row[15]),
    despesa:       parseInt(row[41], 10),
    nome_completo: nome(row[10]),
    nascimento:    misc.nascimento(row[25]),
    sexo:          row[29].toLowerCase(),
    cor_raca:      row[35].toLowerCase(),
    estado_civil:  nome(row[33]),
    escolaridade:  nome(row[31]),
    naturalidade:  nome(row[40]) + ', ' + row[38],
    nacionalidade: nome(row[37])
  };
});

writeJson('./json/candidatos.json', _.sortBy(candidatos, 'despesa').reverse());
