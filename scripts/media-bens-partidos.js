var _ = require('lodash');
var candidatos = require('../json/candidatos');
var totalBens = require('../json/total-bens');
var writeJson = require('../lib/write-json');
require('../lib/round');

var grouped = _.groupBy(candidatos, 'partido');

var averages = _.map(grouped, function(item, partido) {

  var numCandidatos = 0;

  var total = _.reduce(item, function(_total, candidato) {
    if (totalBens[candidato.id] > -1) {
      numCandidatos++;
      return _total += totalBens[candidato.id];
    }
    return _total;
  }, 0);

  return {
    partido: partido,
    total: total,
    media: Math.floor(total / numCandidatos),
    candidatos: numCandidatos,
    taxa: Math.round10(numCandidatos / _.size(item), -2) * 100
  };
});

var sorted = _.sortBy(averages, 'media').reverse();

writeJson('./json/media-bens-partido.json', sorted);
