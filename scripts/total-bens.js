var _ = require('lodash');
var bens = require('../json/bens');
var writeJson = require('../lib/write-json');

var grouped = _.groupBy(bens, 'id_candidato');
var total = {};

_.each(grouped, function(data, id_candidato) {
  total[id_candidato] = _.reduce(data, function(total, next) {
      return total + next.valor;
    }, 0);
});

writeJson('./json/total-bens.json', total);
