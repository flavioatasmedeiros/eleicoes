var parseTxt = require('../lib/parse-txt');
var writeJson = require('../lib/write-json');


writeJson('./json/bens.json', parseTxt('csv/bens/*.txt', function(row) {
  return {
      id_candidato: parseInt(row[5], 10),
      tipo:         row[7],
      descricao:    row[8],
      valor:        parseInt(row[9], 10)
    };
}));
