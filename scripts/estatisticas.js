var _ = require('lodash');
require('../lib/round')
var candidatos = require('../json/candidatos.json');
var writeJson = require('../lib/write-json');

var total = candidatos.length;

function porcento(obj) {
  var p = {};
  _.each(obj, function(valor, chave) {
    if (typeof chave === 'undefined') { return; }
    p[chave] = Math.round10((valor / total) * 100, -1);
  })
  return p;
}


function toList(obj) {
  var list = []
  _.each(obj, function(valor, chave) {
    list.push({
      nome: chave,
      valor: valor
    })
  });
  return list;
}


function group(nome) {
  return _.sortBy(toList(porcento(_.countBy(candidatos, nome))), 'valor').reverse();
}


var stats = {
  cor_raca: group('cor_raca'),
  sexo: group('sexo'),
  partido: group('partido'),
  estado_civil: group('estado_civil'),
  escolaridade: group('escolaridade'),
  situacao: group('situacao'),
};


writeJson('./json/estatisticas.json', stats);
